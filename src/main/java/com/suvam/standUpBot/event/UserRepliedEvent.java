package com.suvam.standUpBot.event;

import com.suvam.standUpBot.StandUpBotApplication;
import com.suvam.standUpBot.entity.DiscordUser;
import com.suvam.standUpBot.entity.UserResponse;
import com.suvam.standUpBot.task.GuildMember;
import com.suvam.standUpBot.task.StandUpScheduler;
import com.suvam.standUpBot.util.DiscordUserInDB;
import com.suvam.standUpBot.util.SaveResponse;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import javax.security.auth.login.LoginException;
import java.util.Calendar;
import java.util.List;

public class UserRepliedEvent extends ListenerAdapter {
    static String userName;
    static int responseCount;
    boolean answered = false;
    String currentAuthor = null;

    @Override
    public void onMessageReceived (@NotNull MessageReceivedEvent event) {

        if (!event.getAuthor().getName().equals(currentAuthor)) {
            answered = false;
        }

        if (event.getMessage().getContentRaw().equals("Work hard and Have a nice day!")) {
            try {
                JDA jda = JDABuilder.createDefault(StandUpBotApplication.token).build().awaitReady();
                TextChannel channel = jda.getGuildsByName("Standup Demo", false).get(0).getTextChannelsByName("general",
                        false).get(0);
                EmbedBuilder info = new EmbedBuilder();
                info.setTitle(userName + "'s" + " Standup Status");

                //traverse through questions and get the respective answers.
                List<String> questions = GuildMember.questions;
                List<UserResponse> responses =
                        DiscordUserInDB.findUserByName(userName).getResponses();
                for (int i = responseCount; i < responses.size(); i++) {
                    info.addField(questions.get(i), responses.get(i).getResponse(), false);
                }
                responseCount = questions.size();
                answered = true;
                info.setColor(0xf45642);
                channel.sendTyping().queue();
                channel.sendMessage(info.build()).queue();
            } catch (LoginException | InterruptedException e) {
                e.printStackTrace();
            }

        } else if (!event.getAuthor().isBot() && !answered) {
            userName = event.getAuthor().getName();
            currentAuthor = userName;
            StandUpScheduler.membersMap.get(userName).startPrivateChat();
            String message = event.getMessage().getContentRaw();
            DiscordUser user = DiscordUserInDB.findUserByName(userName);

            UserResponse response = new UserResponse(message, Calendar.getInstance().getTime());
            response.setUser(user);
            SaveResponse.saveUserResponse(response);
        }
    }
}
