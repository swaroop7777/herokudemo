package com.suvam.standUpBot.service;

import com.suvam.standUpBot.entity.UserResponse;
import java.util.List;

public interface UserResponseService {

    public List<UserResponse> findAll();

    public UserResponse findById(int id);

    public void save(UserResponse response);

    public void deleteById(int id);
}
