package com.suvam.standUpBot.service;

import com.suvam.standUpBot.entity.DiscordUser;
import com.suvam.standUpBot.entity.StandUpQuestion;
import com.suvam.standUpBot.repository.QuestionRepository;
import com.suvam.standUpBot.repository.UserRepository;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DiscordUserServiceImpl implements DiscordUserService{
    private UserRepository userRepository;

    @Autowired
    public DiscordUserServiceImpl (UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<DiscordUser> findAll () {
        return userRepository.findAll();
    }

    @Override
    public DiscordUser findById (int id) {
        Optional<DiscordUser> result = userRepository.findById(id);
        DiscordUser question = null;

        if (result.isPresent()) {
            question = result.get();
        }

        return question;
    }

    @Override
    public void save (DiscordUser question) {
        userRepository.save(question);
    }

    @Override
    public void deleteById (int id) {
        userRepository.deleteById(id);
    }

    @Override
    public DiscordUser findByName (String name) {
        return userRepository.findByName(name);
    }
}
