package com.suvam.standUpBot.service;

import com.suvam.standUpBot.entity.StandUpQuestion;
import com.suvam.standUpBot.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService{

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionServiceImpl (QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public List<StandUpQuestion> findAll () {
        return questionRepository.findAll();
    }

    @Override
    public StandUpQuestion findById (int id) {
        Optional<StandUpQuestion> result = questionRepository.findById(id);
        StandUpQuestion question = null;

        if (result.isPresent()) {
            question = result.get();
        }

        return question;
    }

    @Override
    public void save (StandUpQuestion question) {
        questionRepository.save(question);
    }

    @Override
    public void deleteById (int id) {
        questionRepository.deleteById(id);
    }
}
