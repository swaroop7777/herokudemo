package com.suvam.standUpBot.service;

import com.suvam.standUpBot.entity.StandUpQuestion;
import com.suvam.standUpBot.entity.UserResponse;
import com.suvam.standUpBot.repository.QuestionRepository;
import com.suvam.standUpBot.repository.ResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserResponseServiceImpl implements  UserResponseService{

    private ResponseRepository responseRepository;

    @Autowired
    public UserResponseServiceImpl (ResponseRepository responseRepository) {
        this.responseRepository = responseRepository;
    }

    @Override
    public List<UserResponse> findAll () {
        return responseRepository.findAll();
    }

    @Override
    public UserResponse findById (int id) {
        Optional<UserResponse> result = responseRepository.findById(id);
        UserResponse response = null;

        if (result.isPresent()) {
            response = result.get();
        }

        return response;
    }

    @Override
    public void save (UserResponse response) {
        responseRepository.save(response);
    }

    @Override
    public void deleteById (int id) {
        responseRepository.deleteById(id);
    }
}
