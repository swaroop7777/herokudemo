package com.suvam.standUpBot.service;

import com.suvam.standUpBot.entity.StandUpQuestion;

import java.util.List;

public interface QuestionService {

    public List<StandUpQuestion> findAll();

    public StandUpQuestion findById(int id);

    public void save(StandUpQuestion post);

    public void deleteById(int id);
}
