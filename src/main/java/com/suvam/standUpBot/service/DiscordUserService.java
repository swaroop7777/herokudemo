package com.suvam.standUpBot.service;

import com.suvam.standUpBot.entity.DiscordUser;
import com.suvam.standUpBot.entity.StandUpQuestion;
import net.dv8tion.jda.api.entities.User;

import java.util.List;

public interface DiscordUserService {

    public List<DiscordUser> findAll();

    public DiscordUser findById(int id);

    public void save(DiscordUser post);

    public void deleteById(int id);

    public DiscordUser findByName(String name);
}
