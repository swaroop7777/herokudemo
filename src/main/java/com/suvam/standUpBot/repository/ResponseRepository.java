package com.suvam.standUpBot.repository;

import com.suvam.standUpBot.entity.UserResponse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResponseRepository extends JpaRepository<UserResponse, Integer> {
}
