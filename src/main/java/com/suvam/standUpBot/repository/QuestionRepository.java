package com.suvam.standUpBot.repository;

import com.suvam.standUpBot.entity.StandUpQuestion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<StandUpQuestion, Integer> {

}
