package com.suvam.standUpBot.repository;

import com.suvam.standUpBot.entity.DiscordUser;
import com.suvam.standUpBot.entity.UserResponse;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface UserRepository extends JpaRepository<DiscordUser, Integer> {

    public DiscordUser findByName(String name);

}
