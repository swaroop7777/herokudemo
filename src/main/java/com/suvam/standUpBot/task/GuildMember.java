package com.suvam.standUpBot.task;

import com.suvam.standUpBot.entity.DiscordUser;
import com.suvam.standUpBot.entity.StandUpQuestion;
import com.suvam.standUpBot.util.DiscordUserInDB;
import com.suvam.standUpBot.util.GetQuestions;
import net.dv8tion.jda.api.entities.User;

import java.util.ArrayList;
import java.util.List;

public class GuildMember {
    private User user;
    private int questionCount;
    public static List<String> questions = new ArrayList<>();

    public GuildMember (User user) {
        this.user = user;
        saveUser(user);
        loadQuestions();
        startPrivateChat();
    }

    private void saveUser (User user) {
        if (!DiscordUserInDB.isPresent(user.getName())) {
            DiscordUser discordUser = new DiscordUser(user.getName(), false);
            DiscordUserInDB.saveUser(discordUser);
        }
    }

    private void loadQuestions () {
        List<StandUpQuestion> questionsFromDB = GetQuestions.loadQuestions();
        for (StandUpQuestion question : questionsFromDB) {
            questions.add(question.getQuestion());
        }
    }

    public void startPrivateChat () {

        int questionNumber = questionCount++;
        while (questionNumber < 3) {
            int finalQuestionNumber = questionNumber;
            user.openPrivateChannel().queue(channel -> channel.sendMessage(questions.get(finalQuestionNumber)).queue());
            break;
        }
        if (questionNumber == 3) {
            DiscordUser discordUser = DiscordUserInDB.findUserByName(user.getName());
            discordUser.setQuestions(GetQuestions.loadQuestions());
            discordUser.setHasAnswered(true);
            DiscordUserInDB.saveUser(discordUser);
            user.openPrivateChannel().queue(channel -> channel.sendMessage(questions.get(questionNumber)).queue());
        }
    }
}
