package com.suvam.standUpBot.task;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.User;

import java.util.*;

public class StandUpScheduler extends TimerTask {
    private JDA jda;
    public static Map<String, GuildMember> membersMap;

    public StandUpScheduler (JDA jda) {
        this.jda = jda;
    }

    @Override
    public void run () {
        membersMap = new HashMap<>();
        List<User> user = jda.getUsers();
        Iterator userIterator = user.iterator();
        while (userIterator.hasNext()) {
            User member = (User) userIterator.next();
            if (member.isBot() || member.getJDA().getRoles().contains("admin")) {
                continue;
            } else {
                membersMap.put(member.getName(), new GuildMember(member));
            }
        }
        jda.getPresence().setStatus(OnlineStatus.IDLE);
    }
}
