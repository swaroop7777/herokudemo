package com.suvam.standUpBot.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="questions")
public class StandUpQuestion {

    @Id
    @Column(name = "question_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int qid;

    @Column(name="question")
    private String question;

    @ManyToMany
    @JoinTable(name="user_questions",
            joinColumns = @JoinColumn(name = "qid"),
            inverseJoinColumns = @JoinColumn(name="user_id"))
    private List<DiscordUser> responses;

    public int getQid () {
        return qid;
    }

    public void setQid (int qid) {
        this.qid = qid;
    }

    public String getQuestion () {
        return question;
    }

    public void setQuestion (String question) {
        this.question = question;
    }

    public List<DiscordUser> getResponses () {
        return responses;
    }

    public void setResponses (List<DiscordUser> responses) {
        this.responses = responses;
    }
}
