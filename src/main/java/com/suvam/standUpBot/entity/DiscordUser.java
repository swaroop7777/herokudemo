package com.suvam.standUpBot.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="users")
public class DiscordUser {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userId;

    @Column(name="name")
    private String name;

    @Column(name="has_answered")
    private boolean hasAnswered;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<UserResponse> responses;

    @ManyToMany
    @JoinTable(name="user_questions",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name="qid"))
    private List<StandUpQuestion> questions;

    public DiscordUser () {
    }

    public DiscordUser (String name, boolean hasAnswered) {
        this.name = name;
        this.hasAnswered = hasAnswered;
    }

    public int getUserId () {
        return userId;
    }

    public void setUserId (int userId) {
        this.userId = userId;
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public boolean hasAnswered () {
        return hasAnswered;
    }

    public void setHasAnswered (boolean hasAnswered) {
        this.hasAnswered = hasAnswered;
    }

    public List<UserResponse> getResponses () {
        return responses;
    }

    public void setResponses (List<UserResponse> responses) {
        this.responses = responses;
    }

    public List<StandUpQuestion> getQuestions () {
        return questions;
    }

    public void setQuestions (List<StandUpQuestion> questions) {
        this.questions = questions;
    }
}
