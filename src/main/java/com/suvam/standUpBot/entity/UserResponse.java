package com.suvam.standUpBot.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name="response")
public class UserResponse {

    @Id
    @Column(name = "ans_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="response")
    private String response;

    @Column(name="published_at")
    private Date answeredAt;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH})
    @JoinColumn(name="uid")
    private DiscordUser user;

    public UserResponse () {
    }

    public UserResponse (String response, Date answeredAt) {
        this.response = response;
        this.answeredAt = answeredAt;
    }

    public int getId () {
        return id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getResponse () {
        return response;
    }

    public void setResponse (String response) {
        this.response = response;
    }

    public Date getAnsweredAt () {
        return answeredAt;
    }

    public void setAnsweredAt (Date answeredAt) {
        this.answeredAt = answeredAt;
    }

    public DiscordUser getUser () {
        return user;
    }

    public void setUser (DiscordUser user) {
        this.user = user;
    }

}
