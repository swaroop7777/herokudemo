package com.suvam.standUpBot.util;

import com.suvam.standUpBot.entity.StandUpQuestion;
import com.suvam.standUpBot.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GetQuestions {
    private static QuestionService questionService;

    @Autowired
    public GetQuestions(QuestionService questionService){
        this.questionService = questionService;
    }

    public static List<StandUpQuestion> loadQuestions(){
        return questionService.findAll();
    }
}
