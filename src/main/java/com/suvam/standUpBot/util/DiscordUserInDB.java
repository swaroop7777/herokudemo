package com.suvam.standUpBot.util;

import com.suvam.standUpBot.entity.DiscordUser;
import com.suvam.standUpBot.service.DiscordUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DiscordUserInDB {
    private static DiscordUserService userService;

    @Autowired
    public DiscordUserInDB (DiscordUserService userService) {
        this.userService = userService;
    }

    public static boolean isPresent (String user) {
        if (userService.findByName(user) != null) {
            return true;
        } else {
            return false;
        }
    }

    public static DiscordUser findUserByName (String user) {
        DiscordUser discordUser = userService.findByName(user);

        return discordUser;
    }

    public static void saveUser(DiscordUser user){
        userService.save(user);
    }

}
