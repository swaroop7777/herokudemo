package com.suvam.standUpBot.util;

import com.suvam.standUpBot.entity.UserResponse;
import com.suvam.standUpBot.service.UserResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SaveResponse {
    private static UserResponseService userResponseService;

    @Autowired
    public SaveResponse(UserResponseService userResponseService){
        this.userResponseService = userResponseService;
    }

    public static void saveUserResponse(UserResponse userResponse){
        userResponseService.save(userResponse);
    }
}
